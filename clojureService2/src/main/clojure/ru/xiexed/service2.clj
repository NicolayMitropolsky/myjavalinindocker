(ns ru.xiexed.service2
  (:gen-class)
  (:require [nrepl.server :refer [start-server stop-server]])
  (:import (io.javalin Javalin)
           (io.javalin.http Context Handler)))

(defonce nrepl-server (start-server :bind "0.0.0.0" :port 5555))

(defmacro handler [args body]
  `(reify Handler
     (handle ~(into ['this] args) ~body)))

(defonce server (atom nil))

(defn restart []
  (swap! server
         (fn [old]
           (println "stopping" old)
           (when old
             (.stop old))
           (doto (Javalin/create)
             (.start 7050)
             (.get "/" (handler [ctx] (.result ctx "Hello from Clojure 33")))))))

(defn -main
  [& args]
  (restart))
